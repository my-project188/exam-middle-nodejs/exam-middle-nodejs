const express = require("express");
const { signinMiddlewareTime, signinMiddlewareUrl } = require("../middleware/SignInMiddleware");
const signinRouter = express.Router();

signinRouter.use(signinMiddlewareTime);
signinRouter.use(signinMiddlewareUrl);

module.exports = signinRouter;