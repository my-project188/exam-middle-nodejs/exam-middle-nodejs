const express = require("express");
const app = express();
const port = 8000;
const path = require("path");
const signinRouter = require("./app/router/SignInRouter");

app.use(signinRouter);

app.get("/signin", (req, res) => {
    res.sendFile(path.join(__dirname + "/views/index.html"));
});

app.listen(port, () => {
    console.log("App listening on port: ", port);
});